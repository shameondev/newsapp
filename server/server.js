const express = require('express');
const path = require('path');
const app = express();

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET, HEAD, OPTIONS, POST, PUT');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
  
    next();
  });

app.get('/request1/', (req, res) => {
    res.sendFile(path.resolve(__dirname, './response1.json'), )
});
app.get('/request2/', (req, res) => {
    res.sendFile(path.resolve(__dirname, './response2.json'), )
});
app.get('/request3/', (req, res) => {
    res.sendFile(path.resolve(__dirname, './response3.json'), )
});
app.get('/request4/', (req, res) => {
    res.sendFile(path.resolve(__dirname, './response4.json'), )
});
app.get('/request5/', (req, res) => {
    res.sendFile(path.resolve(__dirname, './response5.json'), )
});
app.get('/request6/', (req, res) => {
    res.sendFile(path.resolve(__dirname, './response6.json'), )
});
app.get('/request7/', (req, res) => {
    res.sendFile(path.resolve(__dirname, './response7.json'), )
});
app.get('/request8/', (req, res) => {
    res.sendFile(path.resolve(__dirname, './response8.json'), )
});
app.get('/request9/', (req, res) => {
    res.sendFile(path.resolve(__dirname, './response9.json'), )
});
app.get('/request10/', (req, res) => {
    res.sendFile(path.resolve(__dirname, './response10.json'), )
});

app.get('/', (req, res) =>{
    res.status(200).json({
        massege: "Working",
    })
})

app.listen(4040, ()=> console.log('server started'))