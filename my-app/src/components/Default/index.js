import React from "react";
import Img from "./img/news.jpg";
import './index.css';
import Vendor from './img/Vendor.jpg';
import zakharov from './img/zakharov.jpg';
import pertzeva from './img/pertzeva.jpg';
import fadeev from './img/fadeev.jpg';
import polonskiy from './img/polonskiy.jpg';

function Default() {
    return (
        <div>
            <div className="newsimg">
                <img src={Img} />
            </div>
            <div className="popular">
                <h3>популярные пользователи</h3>
                <div className='pepole'>
                    <div className='human'><img src={Vendor}></img></div>
                    <div className='human'><img src={zakharov}></img></div>
                    <div className='human'><img src={pertzeva}></img></div>
                    <div className='human'><img src={fadeev}></img></div>
                    <div className='human'><img src={polonskiy}></img></div>
                </div>
            </div>
        </div>
    )
}

export default Default;