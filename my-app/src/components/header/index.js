import React from 'react';
import {
  Link,
} from "react-router-dom";
import './index.css';



function Header({ username, logout }) {
  let path='';
  if (username){
    path = '/default'
  }else path = '/';
  return (
    <div className="header">
      <div className="header_container">
        <Link to={path} className="link"><div className='logo-container'>
          <div className='logo'>SN</div>
          <div className='logotext'>SimpleNews</div>
        </div>
        </Link>
        <div className="settings">{}</div>
        {username ? <div>{username}
          <Link to='/default'>
            <button onClick={logout}>Logout</button>
          </Link>
        </div> :
          <Link to="/login">
            <div className="Login">Login</div>
          </Link>}
      </div>
    </div >
  )
}

// ========================================

export default Header;
