import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import './index.css';
import Header from '../header';
import MainScreen from '../MainScreen';
import OpenedCategory from "../OpenedCategory";
import { Login } from "../loginForm/index";
import Default from "../Default";
import {api} from "../Api/api";


function App() {
 // const 
 const [username, setUsername] = React.useState(api.currentUser()?.username);
 const login = () => setUsername(api.currentUser()?.username);
 const logout = ()=>{
   setUsername(null);
   api.logout();
 }  
  return (
    <Router>
      <Header username={username} logout={logout} />
      <div className="screen container">
        <Switch>
          <Route path='/' exact>
            <Default />
          </Route>
          <Route path="/default" >
            <MainScreen />
          </Route>
          <Route path="/login" component={Login} >
            <Login onLogin={login}/>
          </Route>
          <Route path="/category">
            <OpenedCategory />
          </Route>
        </Switch>
      </div>
    </Router>
  )

}

// ========================================

export default App;
