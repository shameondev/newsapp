import Sorce from "../sorce";

export const api = {
    register: ({ email, username, password }) => {
        setUser({ email, username, password, loggedIn: true });
    },
    login: ({ username, password }) => {
        const user = getUser(username);
        console.log({ user });
        if (!user) {
            console.log("User does not exist");
            return false;
        }
        if (user.password !== password) {
            console.log("Wrong password");
            return false;
        }
        setUser({ username, loggedIn: true });
        return true;
    },
    logout: () => {
        setUser({ ...getLoggedInUser(), loggedIn: false });
    },
    currentUser: () => getLoggedInUser(),

    addUserCategory: (category) => {
        const user = getLoggedInUser();
        let currentCategories = [];
        if (!user) {
            return;
        } if (user.categories) {
            currentCategories = user.categories;
        }
        currentCategories.push({ category: category, sorces: [] });
        setUser({ username: user.username, categories: currentCategories });
    },

    removeUserCategory: (category) => {
        const user = getLoggedInUser();
        if (!user) {
            return;
        }
        const currentCategories = user.categories || [];
        const newCategories = currentCategories.filter((item) => item.category !== category);
        setUser({ username: user.username, categories: newCategories });
    },

    dragDropSort:(arr)=>{
        const user = getLoggedInUser();
        let newCategories =[];
        for(let i = 0; i < arr.length; i++){
            newCategories[i] = (user.categories.filter((item) => item.category === arr[i]))[0];
        }
        //user.categories = newCategories;
        setUser({ username: user.username, categories: newCategories });
    },

    getUserCategories: () => {
        const user = getLoggedInUser();
        let categories = [];
        if (!user) {
            categories = [];
        } else if (user.categories) {
            user.categories.forEach(element => {
                categories.push(element.category);
            });
        } else categories = [];
        return categories;

    },

    addCategorySourse: (name, source) => {
        const user = getLoggedInUser();

        for (let i = 0; i < user.categories.length; i++) {
            console.log(user.categories[i].category);
            if (user.categories[i].category === name) {
                user.categories[i].sorces.push(source);
                setUser({ username: user.username, categories: user.categories });
            }
        }
    },
    removeCategorySourse: (name, source) => {
        const user = getLoggedInUser();

        for (let i = 0; i < user.categories.length; i++) {
            console.log(user.categories[i].category);
            if (user.categories[i].category === name) {
                console.log(user.categories[i].sorces);
                user.categories[i].sorces = user.categories[i].sorces.filter((item) => item !== source);
                console.log(user.categories[i].sorces);
                setUser({ username: user.username, categories:  user.categories });
            }
        }
    },

    getCategorySources: (name) => {
        const user = getLoggedInUser();
        let sorces = [];
        for (let i = 0; i < user.categories.length; i++) {
            if (user.categories[i].category === name) {
                sorces = user.categories[i].sorces;
                console.log(sorces);
            }
        }
        return sorces;
    }
}
function getUserCategories() {
    const user = getLoggedInUser();
    let categories = [];
    if (!user) {
        categories = [];
    } else if (user.categories) {
        user.categories.forEach(element => {

            categories.push(element.category);
        });
    } else categories = [];
    return categories;

}

function getUser(_username) {
    const users = load();
    console.log({ type: typeof users, users })
    if (!users) {
        return null;
    }
    return users.filter(({ username }) => username === _username)[0];
}

function setUser(user) {
    console.log("Set user: ", { user });
    if (!user) {
        return;
    }
    if (!user.username) {
        return;
    }
    const users = load();
    if (!users) {
        save([user]);
        return;
    }
    const existingUser = users.filter(({ username }) => username === user.username);
    if (existingUser.length === 0) {
        save([...users, user]);
        return;
    }
    const updatedUser = { ...existingUser[0], ...user };
    let otherUsers = users.filter(({ username }) => username !== user.username);
    if (user.loggedIn) {
        otherUsers = otherUsers.map((_) => ({ ..._, loggedIn: false }));
    }
    save([...otherUsers, updatedUser]);
}

function getLoggedInUser() {
    const user = load()?.filter(({ loggedIn }) => loggedIn)[0];
    console.log({ loggedInUser: user });
    return user;
}

function load() {
    const data = localStorage.getItem("users");
    if (!data) {
        return [];
    }
    return JSON.parse(data);
}


function save(data) {
    console.log("Saving data: ", { data });
    localStorage.setItem("users", JSON.stringify(data));
}


