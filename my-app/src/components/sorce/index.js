import React from 'react';
import './index.css';




class Sorce extends React.Component {
  constructor() {
    super();
    this.state = {
      error: null,
      isLoaded: false,
      sorceName: "",
      sorce:'',
      items: [],
    }
  }

  componentDidMount() {
    fetch(this.props.sorce)
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            items: result.items,
            sorceName: result.name,
            sorce: this.props.sorce
          });
        },
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
  }
handleClick(){
 // (this.state.sorce);
}
  render() {
    const { error, isLoaded, items, sorceName } = this.state;
    if (error) {
      return <div>Ошибка</div>;
    } else if (!isLoaded) {
      return <div>Загрузка...</div>;
    } else {
      return (
        <div className = 'sorce'>
          <h3>{sorceName}</h3>
          <ul>
            {items.map(item => (
              <li key={item.id}>
                <a href={item.sorce}>{item.name}</a>
              </li>
            ))}
          </ul>
          <button onClick={() => {this.props.delete(this.state.sorce)}}>удалить</button>
        </div>
      );
    }
  }
}


// ========================================

export default Sorce;
