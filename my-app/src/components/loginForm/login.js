import React from "react";
import loginImg from "../loginForm/img/loginImg.jpg";
import { useRef, useState } from "react";
import {useHistory} from "react-router-dom";
import {api} from "../Api/api";

export function Login({onLogin}) {
    const history = useHistory();
    const [formTouched, setFormTouched] = React.useState(false);
    const [username, setUsername] = React.useState('');
    const [password, setPassword] = React.useState('');
    const [email, setEmail] = React.useState('');
    const [registerMode, setRegisterMode] = React.useState(false);
    const containerRef = useRef(null)
    const formDefaultErrors ={
        username: [],
        password: [],
        email: []
    }
    const [formErrors, setFormErrors] = useState(formDefaultErrors);
    
    const handleInput = (callBack, value)=>{
        callBack(value);
        setFormTouched(true);
    }

    const resetForm=()=>{
        setFormErrors(formDefaultErrors);
        setPassword('');
        setUsername('');
        setEmail('');
        setFormTouched(false);
    }

    console.log(formErrors);

    return (
        <div className="App">
            <div className="login">
                <div className="container" >
                    <div className="base-container" ref={containerRef}>
                        <div className="login-header">{registerMode ? "Register" :  "Login"}</div>
                        <div className="content">
                            <div className="image">
                                <img src={loginImg} />
                            </div>
                            <div className="login-form">
                                <InputField
                                    key="username" 
                                    value={username}
                                    name="username" 
                                    type="text" 
                                    onChange={(v)=>handleInput(setUsername, v)}
                                    validate={(target) =>handleValidations(target, [noBlanks, moreThen3, lessThen13, notEmpty ], setFormErrors, formErrors)} 
                                    error={formErrors['username']} 
                                />
                                {registerMode && <InputField 
                                    key="email" 
                                    value={email}
                                    name="email" 
                                    type="text" 
                                    onChange={(v)=>handleInput(setEmail, v)}
                                    validate={(target) =>handleValidations(target, [noBlanks, validEmail, notEmpty ], setFormErrors, formErrors)}
                                    error={formErrors['email']} 
                                />}
                                <InputField
                                    key="password" 
                                    value={password}
                                    name="password" 
                                    type="password"
                                    onChange={(v)=>handleInput(setPassword, v)}
                                    error={formErrors['password']} 
                                    validate={(target) => handleValidations(target, [noBlanks, moreThen3, lessThen13, notEmpty ], setFormErrors, formErrors)}
                                />
                            </div>
                        </div>
                        <div className="login-footer">
                            <button
                                type="button"
                                className="btn"
                                disabled={!formTouched }
                                onClick={() => {
                                    const invalidUserNameOrPassword  = 'Неправильное имя пользователя или пароль';
                                    setFormErrors(prevState => ({
                                        ...prevState,
                                        login: formErrors?.login?.filter(error=>error !== invalidUserNameOrPassword)
                                    }))
                                    const valid = isValid({formErrors, username, email, password, setFormErrors, registerMode });
                                    if(!valid){ 
                                        return;
                                    } 
                                    if(registerMode){
                                        api.register({email, password, username})
                                        onLogin();
                                        history.push('/default');
                                    } else {
                                        if( api.login({username, password})){
                                            onLogin();
                                            history.push('/default');
                                        }
                                        else{
                                            setFormErrors(prevState => ({
                                                ...prevState,
                                                login: [invalidUserNameOrPassword]
                                            }))
                                        }
                                        
                                    }
                                }}
                            >
                                {registerMode ? "Register" : "Login"}
                            </button>
                            {!!formErrors.login?.length && <div style={{color:'red'}}>{formErrors.login[0]}</div>}
                        </div>
                    </div>
                </div>
                <RightSide
                    onClick={() => {
                        resetForm();
                        setRegisterMode((prev) => !prev);
                    }}
                    current={registerMode}
                    containerRef={containerRef}
                />
            </div>
        </div>
    )
}

function RightSide({ onClick, current, containerRef }) {
    return (
        <div
            className={`right-side ${current ? 'left' : 'right'}`}
            onClick={onClick}
            ref={containerRef}
        >
            <div className="inner-container">
                <div className="text">{current ? "Login" : "Register"}</div>
            </div>
        </div>
    );
}

function InputField({ name, onChange, type, error, validate, value }) {
    return (<div className="form-group">
        <label htmlFor={name}>{name.charAt(0).toUpperCase() + name.slice(1)}</label>
        <input
            value={value}
            type={type}
            name={name}
            placeholder={name}
            onChange={(({ target }) => onChange(target.value))}
            onBlur={(e) => validate(e.target)}
        />
        {error && <div style={{color:'red'}}>{error.map(e=><div>{e}</div>)}</div>}
    </div>)
}


function noBlanks(value){
    return{
        valid: value.replace(/\s/, "").length >0,
        message: "Обязательное поле"
    }
}

function validEmail(value){
    return{
        valid: value.split("@").length>1,
        message: "Введите корректный email"
    }
}

function moreThen3(value){
        return{
            valid: value.length >3,
            message: "Минимальная длина 4 символа "
    }
}
function lessThen13(value){
    return{
         valid: value.length <34,
         message: "Максимальная длина 33 символов "
    }
}

function notEmpty(value){
    return{
         valid: value.length >0,
         message: "Обязательное поле"
    }
}


function handleValidations(target, validators, setFormErrors, formErrors){
    validators.forEach(validation => {
        const result = validation(target.value);
        const errors = formErrors[target.name];
        console.log({formErrors, errors, target})
        if(!errors){
            return;
        }
        if(result.valid){
            if(errors?.includes(result.message)){
                setFormErrors(prevState => ({
                    ...prevState,
                    [target.name]: errors.filter(error=>error !== result.message)
                }))
            }
        }else{
            addErrorIfNotIncluded(formErrors, setFormErrors, target.name, result.message);
    }});
}
function isValid({formErrors, username, email, password, setFormErrors, registerMode }){
    const cantbeBlank = "Обязательное поле";
    if(!username){
        addErrorIfNotIncluded(formErrors, setFormErrors, "username", cantbeBlank);
        return false;
    }
    if(!email && registerMode){
        addErrorIfNotIncluded(formErrors, setFormErrors, "email", cantbeBlank);
        return false;
    }
    if(!password){
        addErrorIfNotIncluded(formErrors, setFormErrors, "password", cantbeBlank);
        return false;
    }
    for(let field in formErrors){
        if(field==="login"){
            continue;
        }
        if(formErrors[field].length>0){
            return false;
        }
    }
    return true;
}

function addErrorIfNotIncluded(formErrors, setFormErrors, errorKey, errorMessage ){
    const errors = formErrors[errorKey];
    if(!errors?.includes(errorMessage)){
        setFormErrors(prevState => ({
            ...prevState,
                [errorKey]: [...errors, errorMessage]
            })) 
    }
}

