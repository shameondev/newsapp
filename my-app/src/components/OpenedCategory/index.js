import React from 'react';
import './index.css';
import AddSorceForm from "../AddSorceForm";
import Sorce from '../sorce';
import {api} from '../Api/api';


class OpenedCategory extends React.Component {
  constructor() {
    super();
    this.state = {
      sorces: [],
      name: '',
      adding: false,
    }
  }
  setSorceyName = (name) => {
    let sorces = this.state.sorces;
    sorces.push(name);
    this.setState({
      sorces: sorces,
      adding: false
    });
    api.addCategorySourse(this.state.name, name);

  }
  componentDidMount() {
    let categoryName = localStorage.getItem('openedCategory');
    if(categoryName === ''){}
    this.setState({ name: categoryName });
    console.log(categoryName);
    let sorces = api.getCategorySources(categoryName);//localStorage.getItem(categoryName);
    console.log(sorces);
    if (sorces) {
      this.setState({ sorces: sorces/*.split(',') */});
    }
  }
  handlClick = () => {
    this.setState({ adding: true })
  }

  handleDeleteElement = (sorce) => {
    let categoryName = localStorage.getItem('openedCategory');
    let sorces = this.state.sorces;
    sorces = sorces.filter((item) => item !== sorce);
    this.setState({ sorces: sorces });
    api.removeCategorySourse(categoryName, sorce);//localStorage.setItem(categoryName, sorces);
  }

  render() {
    let screen = null;
    if (this.state.adding) {
      screen = <AddSorceForm props={this.setSorceyName} />
    } else {
      let arr = this.state.sorces;
      if (arr) {
        screen = arr.map((sorce) => <Sorce sorce={sorce} key={sorce} delete={this.handleDeleteElement} />);
      }
    }


    return (
      <div>
        <div className='flex '>
          <h2>{this.state.name}</h2>
          <button onClick={this.handlClick} >добавить источник</button>
        </div>
        <div className='flex'>
          {screen}
        </div>
      </div>
    )
  }

}

// ========================================

export default OpenedCategory;
