import React from 'react';
import './index.css';
import {api} from '../Api/api'



class AddForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = { value: '' };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({ value: event.target.value });
  }

  handleSubmit(event) {
    let categories = api.currentUser().categories;
    console.log(categories);
    let flag = false;
    let value = this.state.value;
    if (categories) {
      //categories = categories.split(',');
      categories.forEach(function (item) {
        if (item === value) {
          flag = true;
          return
        }
      });
      if (flag) {
        alert('ТАКАЯ КАТЕГОРИЯ УЖЕ СУЩЕСТВУЕТ');
      } else this.props.props(this.state.value);
    } else this.props.props(this.state.value);

    event.preventDefault();
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <label>
          <input type="text" value={this.state.value} onChange={this.handleChange} placeholder="Имя категории" />
        </label>
      </form>
    );
  }
}

export default AddForm;