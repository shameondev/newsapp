import React from 'react';
import './index.css';
import Mainscreen from '../MainScreen';




class Category extends React.Component {
  constructor() {
    super();
    this.state = {
      categoryName: '',
      key: '',
      deletebutton: false,
      deleteclass: 'deleteclass'
    }
    this.handleClick = this.handleClick.bind(this);
  }

  dragStart = e => {
    localStorage.setItem('dragTake', this.props.id);
  }

  dragOver = e => {
    e.preventDefault();
  }

  handleDrop = e => {
    e.preventDefault();
    if (this.props.id !== undefined) {
      this.props.onDrop(this.props.id);
    }
  }

  componentDidMount() {
    this.setState({
      categoryName: this.props.title,
    });
  }


  handleClick = () => {
    if (this.state.categoryName === 'Add Category') {
      this.props.newCategory();
    } else {
      localStorage.setItem('openedCategory', this.state.categoryName);
      console.log('vtyz кликнули');
    }
  }
  handleMousEnter = () => {
    if(this.state.categoryName!=='Add Category'){
    this.setState({ deleteclass: 'deleteclass show' });
    }

  }
  handleMouseLeave = () => {
    this.setState({ deleteclass: 'deleteclass' })
  }
  handleDelete = (e) =>{
    e.stopPropagation();
    e.preventDefault();
    this.props.deleteCategory(this.state.categoryName);
    
  }

  render() {
    let classes = '';
    if (this.props.className) {
      classes = `category ${this.props.className}`
    } else classes = 'category'

    return (
      <div onDrop={this.handleDrop} onDragOver={this.dragOver} onDragStart={this.dragStart} draggable={this.props.draggable} className={classes} onClick={this.handleClick} onMouseEnter={this.handleMousEnter} onMouseLeave={this.handleMouseLeave}>
        <div className="category_icon"> <button className={this.state.deleteclass} onClick={this.handleDelete}>x</button></div>
        <div className="title">{this.props.title}</div>
      </div>
    )
  }
}

// ========================================

export default Category;
