import React from 'react';
import './index.css';



class AddSorceForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = { value: 'http://localhost:4040/request1/' };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({ value: event.target.value });
  }

  handleSubmit(event) {
    this.props.props(this.state.value);
    event.preventDefault();
  }
 
  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <select name="sorceSelect" onChange={this.handleChange} value={this.state.value}>
          <option value='http://localhost:4040/request1/' >сайт 1</option>
          <option value='http://localhost:4040/request2/'>сайт 2</option>
          <option value='http://localhost:4040/request3/'>сайт 3</option>
          <option value='http://localhost:4040/request4/'>сайт 4</option>
          <option value='http://localhost:4040/request5/'>сайт 5</option>
          <option value='http://localhost:4040/request6/'>сайт 6</option>
          <option value='http://localhost:4040/request7/'>сайт 7</option>
          <option value='http://localhost:4040/request8/'>сайт 8</option>
          <option value='http://localhost:4040/request9/'>сайт 9</option>
          <option value='http://localhost:4040/request10/'>сайт 10</option>
        </select>
        <button>добавить</button>
      </form>
    );
  }
}

export default AddSorceForm;